from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}         # noqa Create a dictionary for the headers to use in the request
    params = {                                          # noqa The parameters for the search, found in Pexel documentation. Query is required
        "query": city + ' ' + state,                    # noqa The query is here instead of url below because it can change from instance to instance, shouldnt be hard-coded
        "per_page": 1
    }
    url = "https://api.pexels.com/v1/search"            # noqa Create the URL for the request. Found in Pexel documentation
    response = requests.get(url, params=params, headers=headers)    # noqa Make the request, this syntax is found in request documentation, including adding params
    content = json.loads(response.content)              # noqa Parse the JSON response
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}    # noqa Return a dictionary that contains a `picture_url` key and a value of specs found in the Pexel doc
    except(KeyError, IndexError):
        return {"picture_url": None}
# print(get_photo("eugene", "oregon"))


def get_weather_data(city, state):
    params = {
        "q": f"{city},{state},US",
        "appid": OPEN_WEATHER_API_KEY,
        "limit": 1,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"        # noqa Create the URL for the geocoding API with the city and state
    response = requests.get(url, params=params)                 # noqa Make the request, include the parameters
    content = json.loads(response.content)                      # noqa Parse the JSON response
    try:                                                        # noqa Get the latitude and longitude from the response
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except(KeyError, IndexError):
        return None

    params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    url = "https://api.openweathermap.org/data/2.5/weather"    # noqa Create the URL for the current weather
    response = requests.get(url, params=params)                # noqa make the request, include params
    content = json.loads(response.content)                     # noqa Parse the JSON response
    try:
        return {                                               # noqa return a dictionary with the temp and description
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"]
        }
    except(KeyError, IndexError):
        return None
# print(get_weather_data("eugene", "oregon"))
